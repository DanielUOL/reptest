#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "orozcolomeli.h"
///////////////////////////////////////////////////////////////////////////
#define RI 347000
#define RS 349499
#define ESC 27
#define N 100
///////////////////////////////////////////////////////////////////////////
typedef struct _talum{
	long matri;
	int edad, sexo;
	char appat[30],apmat[30],nombre[30],estado[4],anio[5],mes[3],dia[3],curp[30];	
}Talum;
///////////////////////////////////////////////////////////////////////////////
void AgregarBinario(char *data,int n);
int BuscarRegBinario(Talum *alum,long matri,char *data);
Talum agregardatos(void);
void LeerBinario(char *data);
void OrdenarBinario(char *data);
void OrdenarVector(Talum alumorden[],int tam);
//////////////////////////////////////////////////////////////////////////////
int main(void)
{
	Talum reg; FILE *arch; char data[30],opx[20],op[20]; int cant; long matri;
	validacad(data,"ingresa el nombre del archivo"); 
	arch=fopen(data,"r");
	if(arch)
	  {
	  	while(!feof(arch))
	  	    {
	  	     	fscanf(arch,"%s",&op);
	  	     	if(strcmpi(op,"ADD")==0)
	  	     	  {
	  	     	  	 fscanf(arch,"%s %d",&opx,&cant);
	  	     	  	 AgregarBinario(opx,cant);				 
	  	     	  }
	  	     	 else
	  	     	  {
	  	     	  	 if(strcmpi(op,"PRINT")==0)
	  	     	  	   {
	  	     	  	   	fscanf(arch,"%s",&opx);
						LeerBinario(opx);
	  	     	  	   }
	  	     	  	  else
	  	     	  	   {
	  	     	  	   	  if(strcmpi(op,"SORT")==0)
	  	     	  	   	    {
							fscanf(arch,"%s",&opx);
							OrdenarBinario(opx);
	  	     	  	   	    }
	  	     	  	   	   else
	  	     	  	   	    {
	  	     	  	   	      if(strcmpi(op,"FIND")==0)
	  	     	  	   	      {
	  	     	  	   	      	fscanf(arch,"%ld %s",&matri,&opx);
		  	     	  	   	      if(BuscarRegBinario(&reg,matri,opx)!=-1)
		  	     	  	   	        {
		  	     	  	   	        	printf("%ld %s %s %s %d %d\n",reg.matri,reg.appat,reg.apmat,reg.nombre,reg.sexo,reg.edad);
		  	     	  	   	        }
		  	     	  	   	       else
		  	     	  	   	        {
		  	     	  	   	        	printf("alumno no existe\n");
		  	     	  	   	        }
	  	     	  	   	      }
	  	     	  	   	    }
	  	     	  	   }
	  	     	  }
	  	    }
	  }
	 else
	  {
	  	printf("el archivo no existe\n");
	  }
	return 0;
}
/////////////////////////////////////////////////////////////////////////////////////
void AgregarBinario(char *data,int n)
{
	Talum reg;
	int i=0;
	FILE *arch;
	  arch=fopen(data,"r+b");
	if(!(arch))
	  {
	  	arch=fopen(data,"w+b");
	  }
	  fseek(arch,0,SEEK_END);
	  for(i=0;i<n;i++)
	     {
	      do{
	     	 reg=agregardatos();
			}while((BuscarRegBinario(&reg,reg.matri,data))!=-1);
			fwrite(&reg,sizeof(struct _talum),1,arch);	
	     }
	fclose(arch);	
}
/////////////////////////////////////////////////////////////////////////
void LeerBinario(char *data)
{
	Talum reg;
	FILE *arch;
	int i=0;
	system("cls");
	arch=fopen(data,"rb");
	if(arch)
	  {
	  	printf("Indice Matricula Nombre Sexo Edad\n");
	  	while(fread(&reg,sizeof(struct _talum),1,arch))
	  	     {
	  	     	printf("%d %ld %s %s %s %d %d\n",i+1,reg.matri,reg.appat,reg.apmat,reg.nombre,reg.sexo,reg.edad);
	  	     	i++;
	  	     }
	  }
	 else
	  {
	  	printf("el archivo no existe");
	  }
	  fclose(arch);
	  system("pause");
}
/////////////////////////////////////////////////////////////////////////////
int BuscarRegBinario(Talum *alum,long matri,char *data)
{
	Talum reg;
	FILE *arch;
	int i=0,pos=-1,band=0;
	arch=fopen(data,"rb");
	if(arch)
	  {
	  	while(fread(&reg,sizeof(struct _talum),1,arch) && band==0)
	  	     {
	  	    	if(matri==reg.matri)
				  {
				    *alum=reg;
					pos=i;
					band=1;
				  }	
				i++;
	  	     }
	  }
	 else
	  {
	  	printf("el archivo no existe");
	  	system("pause");
	  }
	fclose(arch);
	return pos;
}
//////////////////////////////////////////////////////////////////////////
void OrdenarBinario(char *data)
{
	FILE *arch,*arch2;
	Talum reg;
	char data2[30];
	int i,n;
	arch=fopen(data,"r+b");
	if(arch)
	  {
	  	fseek(arch,0,SEEK_END); 	
		n=(ftell(arch)/sizeof(struct _talum));
		Talum vect2[n];		
		i=0;
		fseek(arch,0,SEEK_SET); 
		while(fread(&reg,sizeof(struct _talum),1,arch))
		     {
		     	vect2[i]=reg;
		     	i++;
		     }	   
	  	OrdenarVector(vect2,n);
	  	i=0;
	  fseek(arch,0,SEEK_SET); 
	  	do{
	  		reg=vect2[i];
	  		fwrite(&reg,sizeof(struct _talum),1,arch);
	  		i++;
	  	}while(i<n);
	  }
     else
      {
      	printf("El archivo no Existe");
      	system("pause");
      }
	fclose(arch);
}
//////////////////////////////////////////////////////////////////////////
void OrdenarVector(Talum alumorden[],int tam)
{
	system("cls");
	int i,j;
	Talum burb;
	for(i=1;i<tam;i++)
	   {
	   	for(j=0;j<tam-1;j++)
	   	   {
	   	   		if(alumorden[j].matri>alumorden[j+1].matri)
	   	   		  {
	   	   		  	    burb=alumorden[j];
					    alumorden[j]=alumorden[j+1];
						alumorden[j+1]=burb;  
	   	   		  }
	   	   }  
	   }
	   printf("done \n");
	system("pause");
}
///////////////////////////////////////////////////////////////////////////
Talum agregardatos(void)
{
	Talum reg;
char nombH[50][30]={"ERNESTO","RAUL","DANIEL","PEDRO","JUAN","LUIS","JESUS",
 "EUGENIO","CARLOS","RAMIRO","MATEO","ESTEBAN","ALVARO","ADRIAN","PABLO",
 "DIEGO","SERGIO","MARIO","GOKU","MARCO","DANTE","EZIO","ARIEL","BENITO",
 "BEN","BENJAMIN","CID","CESAR","DEMIAN","DAVID","EDGAR","EFRAIN","EDMUNDO",
 "FABIAN","FACUNDO","FAUSTO","FELIPE","FEDERICO","GERMAN","GERARDO","GALI",
 "HECTOR","IVAN","ISAAC","JACOB","JAVIER","MARCELO","MIGUEL","ODIN","PABLO"};
 
char nombM[50][30]={"PRISCILA","STEPHANY","KARLA","VALERIA","DAENA","FERNANDA",
 "ROSA","ANA","MONICA","VICTORIA","ABRIL","ADRIANA","ALEJANDRA","ALMA","ALICIA"
 ,"AIDA","AMANDA","BLANCA","BIANCA","BEATRIZ","BERTA","CAROLINA","CINTHIA","CECILIA",
 "CLAUDIA","CARMEN","DAFNE","DOLORES","DORA","DANA","EVA","EMMA","ELENA","ERIKA",
 "ESMERALDA","FLOR","FABIOLA","FATIMA","GRECIA","GLORIA","GLADIS","HILDA","IRIS",
 "INGRID","JAZMIN","JULIA","JIMENA","KAREN","KARINA","LINDA"};
		
char apell[60][20]={"OROZCO","OREGEL","MENDOZA","GUTIERREZ","ORCHAK",
"MARTINEZ","CASTRO","LOZANO","HERNANDEZ","GOMEZ","ESTRADA","PEREZ",
"LOMELI","LOPEZ","ALANIZ","LEON","CHAVEZ","YEPIZ","QUIROZ","FLORES","RUIZ","SOTO",
"MANDUJANO","ESCATELL","ECHAVARRIA","ALARCON","MARQUEZ","CALDERON","FRAGOSO","MENDEZ",
"DIAZ","GARCIA","ANGUIANO","AGUILAR","ARROYO","TOSCANO","CARRILLO","IZAGUIRRE",
"CENTENO","ESPINOZA","CARDENAS","VIZARRA","ORTEGA","RIZO","VAZQUEZ","OJEDA","LARA",
"MORFIN","GODOY","GONZALEZ","BARAJAS","AMARILLAS","CONEJO","MEDINA","IBARRA","ELIZONDO",
"OSORIO","FONSECA","RAMOS","GUZMAN"};
	
reg.sexo=(rand()%100)%2;
reg.matri=(rand()%(RS-RI))+RI;
reg.edad=18+rand()%10;
strcpy(reg.appat,apell[rand()%60]);
strcpy(reg.apmat,apell[rand()%60]);
if(reg.sexo==0)
  {
  	strcpy(reg.nombre,nombH[rand()%50]);
  	if((rand()%50)%2==0)
  	  {
  	  	strcat(reg.nombre,"_");
  	  	strcat(reg.nombre,nombH[rand()%50]);
  	  }
  }
 else
  {
  	strcpy(reg.nombre,nombM[rand()%50]);
  	if((rand()%50)%2==0)
  	  {
  	  	strcat(reg.nombre,"_");
  	  	strcat(reg.nombre,nombM[rand()%50]);
  	  }
  }
	return reg;
}
